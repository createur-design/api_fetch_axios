console.log('DOM READY!');

document.getElementById("blah").style.display = "none";

window.addEventListener("load", () => {
    console.log("DOM LOADED!");  
   
    
    // SHOW IMG INPUT FILE

    function readURL(input) {
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          
          reader.onload = function(e) {
            document.getElementById("blah").style.display = "block";
            document.getElementById("blah").src = e.target.result;
          }
          
          reader.readAsDataURL(input.files[0]);
        }
      }
      
      const inputFile = document.getElementById('fileToUpload');
      inputFile.addEventListener('change', function() {
        readURL(this);
      });

    const form = document.getElementById('formulaire');
    form.addEventListener('submit', function(e) {
        e.preventDefault();
        document.querySelectorAll('.comments').forEach(elem => {
            return elem.innerHTML = "";
        });

        const formData = new FormData(this);            

        // fetch('./php/traitement.php', {
        //    method: 'post',
        //     body: formData
        // })
        // .then(function(response) {
        //     return response.json();
        // })
        // .then(function(result) {
        //     console.log('result:', result);
            
        //     if(!result.isSuccess){
        //         document.getElementById('nom').nextElementSibling.innerHTML = result.errorNom;
        //         document.getElementById('prenom').nextElementSibling.innerHTML = result.errorPrenom;
        //         document.getElementById('email').nextElementSibling.innerHTML = result.errorEmail;
                
        //     } else {
        //         console.log('ok');
                
        //     }

            
        // })
        // .catch((err) => {
        //     console.log(err.message);
            
        // })

        axios({
            method: 'post',
            url: './php/traitement.php',
            data: formData
          })
          .then(response => {
              console.log(response);
              if(!response.data.isSuccess){
                    document.getElementById('nom').nextElementSibling.innerHTML = response.data.errorNom;
                    document.getElementById('prenom').nextElementSibling.innerHTML = response.data.errorPrenom;
                    document.getElementById('email').nextElementSibling.innerHTML = response.data.errorEmail;
                    document.getElementById('fileToUpload').nextElementSibling.innerHTML = response.data.errorFile;
                    
                } else {
                    console.log('ok'); 
                    form.reset();  
                    document.getElementById("blah").style.display = "none";                 
                }
              
          })
          .catch(err => {
              console.error(err);
              
          })
        
        
    })
    


})