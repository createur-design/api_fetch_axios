<?php

    $array = [
        "nom" => "",
        "prenom" => "",
        "email" => "",
        // "phone" => "",
        // "message" => "",
        "errorNom" => "",
        "errorPrenom" => "",
        "errorEmail" => "",
        "errorFile" => "",
        // "phoneError" => "",
        // "messageError" => "",
        "isSuccess" => false,
    ];
    
    $emailTo = "cdelobel.ext@simplon.co";

    if ($_SERVER["REQUEST_METHOD"] == "POST") 
    { 
        $array["nom"] = htmlspecialchars($_POST["nom"]);
        $array["prenom"] = htmlspecialchars($_POST["prenom"]);
        $array["email"] = htmlspecialchars($_POST["email"]);
        $array['file'] = $_FILES['fileToUpload'];
        $array["isSuccess"] = true; 
        $emailText = "";
        
        if (empty($array["nom"])) {
            $array["errorNom"] = "Je veux connaitre ton nom !";
            $array["isSuccess"] = false; 
        } else {
            $emailText .= "Nom : {$array['nom']}\n";
        }

        if (empty($array["prenom"])) {
            $array["errorPrenom"] = "Et oui je veux tout savoir. Même ton prénom !";
            $array["isSuccess"] = false; 
        } else {
            $emailText .= "Prénom : {$array['prenom']}\n";
        }

        if(!isEmail($array["email"])) {
            $array["errorEmail"] = "T'essaies de me rouler ? C'est pas un email ça  !";
            $array["isSuccess"] = false; 
        } else {
            $emailText .= "Email : {$array['email']}\n";
        }

        ////////////////////////////////////////////////////////
        // traitement le l'image

        $target_dir = "../uploads/";

        if(!is_dir($target_dir)){
            mkdir($target_dir, 0700);
        } 

        if ($_FILES['fileToUpload']['size'] == 0) {
            // cover_image is empty (and not an error)
            $array['errorFile'] = "cover_image is empty (and not an error)";
            $array["isSuccess"] = false;             
        }   

        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image

        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            $array['errorFile'] = "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            $array['errorFile'] = "File is not an image.";
            $uploadOk = 0;
            $array["isSuccess"] = false; 
        }

        // Check if file already exists
        if (file_exists($target_file)) {
            $array['errorFile'] = "Sorry, file already exists.";
            $uploadOk = 0;
            $array["isSuccess"] = false;
        }
        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            $array['errorFile'] = "Sorry, your file is too large.";
            $uploadOk = 0;
            $array["isSuccess"] = false; 
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $array['errorFile'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
            $array["isSuccess"] = false; 
        }        


        ////////////////////////////////////////////////////////

        // if (!isPhone($array["phone"]))
        // {
        //     $array["phoneError"] = "Que des chiffres et des espaces, stp...";
        //     $array["isSuccess"] = false; 
        // }
        // else
        // {
        //     $emailText .= "Phone: {$array['phone']}\n";
        // }

        // if (empty($array["message"]))
        // {
        //     $array["messageError"] = "Qu'est-ce que tu veux me dire ?";
        //     $array["isSuccess"] = false; 
        // }
        // else
        // {
        //     $emailText .= "Message: {$array['message']}\n";
        // }
        
        if($array["isSuccess"]) {


            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                // $array['errorFile'] = "Sorry, your file was not uploaded.";
                $array["isSuccess"] = false; 
            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                    $array['errorFile'] = "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                } else {
                    $array['errorFile'] = "Sorry, there was an error uploading your file.";
                    
                }
            }

            $headers = "From: {$array['nom']} {$array['prenom']} <{$array['email']}>\r\nReply-To: {$array['email']}";
            // mail($emailTo, "Un message de votre site", $emailText, $headers);
        }
        
        echo json_encode($array);
        
    }

    function isEmail($email) 
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
    // function isPhone($phone) 
    // {
    //     return preg_match("/^[0-9 ]*$/",$phone);
    // }
 
?>